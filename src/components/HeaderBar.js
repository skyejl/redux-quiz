import React from 'react';
import { MdAssignment } from "react-icons/md";
import {Link} from 'react-router-dom';

const HeaderBar = () => {
    return (
        <header>
            <p><Link to="/"><MdAssignment className="navIcon"/></Link>
                NOTES
            </p>
        </header>
    );
}
export default HeaderBar;
