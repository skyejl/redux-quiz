import React, {Component} from 'react';
import HeaderBar from "./HeaderBar";
import '../style/note.less'
class NewNote extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            note: {
                title: '',
                desc: ''
            }
        };
        this.create = this.create.bind(this);
        this.bindNote = this.bindNote.bind(this);
    }

    render() {
        return (
            <div className="create-note">
                <HeaderBar></HeaderBar>
                <p className="create-note-title">创建笔记</p>
                <hr/>
                <form action="">
                    <label htmlFor="title">标题</label>
                    <input name="title" type='text' onChange={e => this.bindNote(e, 'title')}/>
                    <label htmlFor="title">正文</label>
                    <textarea name="desc" id="" cols="30" rows="10" onChange={e => this.bindNote(e, 'desc')}></textarea>
                    <div className="button-group">
                        <button onClick={this.create} className="button-delete">提交</button>
                        <button type="reset" className="button-back">取消</button>
                    </div>
                </form>
            </div>
        );
    }

    create(event) {
        event.preventDefault();
        const {history} = this.props;
        const {title, desc}  = this.state.note;
        fetch('http://localhost:8080/api/posts', {
            method: 'POST',
            headers: {'Content-Type': 'application/json;charset=utf-8'},
            body: JSON.stringify({title: title, description: desc})
        }).then(() => {
            history.push('/')
        })
    }

    bindNote(e, key) {
        const {note} = this.state;
        if (key === 'title') {
            this.setState({
                note: {
                    ...note,
                    title: e.target.value
                }
            })
        } else {
            this.setState({
                note: {
                    ...note,
                    desc: e.target.value
                }
            })
        }
    }
}

export default NewNote;
