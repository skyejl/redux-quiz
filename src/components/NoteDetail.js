import React, {Component} from 'react';
import {getNoteDetails} from "../actions/details";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Link} from "react-router-dom";
import NoteContent from "./NoteContent";
import HeaderBar from "./HeaderBar";
import '../style/detail.less'


class NoteDetail extends Component {

    componentDidMount() {
        this.props.getNoteDetails();
    }
    render() {
        const details = this.props.payload;
        return (
            <div className="note-detail">
                <HeaderBar></HeaderBar>
                <ul className="detailNav">
                    {
                        details.map(item => <li key={item.id}>
                            <Link to={`/notes/${item.id}`}>{item.title}</Link>
                        </li>)
                    }
                </ul>
                <NoteContent details = {this.props.payload} id = {this.props.match.params.id} history={this.props.history}/>

            </div>
        );
    }
}
const mapStateToProps = state => ({
    payload: state.details.payload
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getNoteDetails
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NoteDetail);
