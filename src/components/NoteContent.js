import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteDetails} from "../actions/details";
import {bindActionCreators} from "redux";
import '../style/detail.less'
import {Link} from "react-router-dom";


class NoteContent extends Component {
    constructor(props, context) {
        super(props, context);
        this.delete = this.delete.bind(this);
    }
    render() {
        const {details = [], history} = this.props;
        const id = this.props.id;
        let note = details.filter(item => item.id === parseInt(id))[0];
        if (note === undefined) {
            note = {};
        }
        return (
            <div className="right-part">
                <p className="content-title">{note.title}</p>
                <hr className="content-partition"/>
                <p className="content-description">{note.description}</p>
                <hr className="content-partition"/>
                <div className="button-group">
                    <button onClick={() => this.delete(note.id, history)} className="button-delete">删除</button>
                    <button className="button-back"><Link to="/">返回</Link></button>
                </div>
            </div>
        );
    }
    delete(id, history) {
        this.props.deleteDetails(id, history);
    }
}
const mapDispatchToProps = dispatch => bindActionCreators({
    deleteDetails
}, dispatch);

export default connect(mapDispatchToProps)(NoteContent);
