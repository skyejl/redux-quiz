const initialState = {
    payload: []
};
export default function detailsReduces(state = initialState, action) {
    switch (action.type) {
        case 'GET_DETAILS':
            return {
                ...state,
                payload: action.payload
            };
        case 'DELETE_DETAILS':
            return {
                ...state,
            };
        default:
            return state
    }
}
