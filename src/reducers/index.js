import {combineReducers} from "redux";
import detailsReduces from "./detailsReduces";

const reducers = combineReducers({
    details: detailsReduces
});
export default reducers;
