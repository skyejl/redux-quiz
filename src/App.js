import React, {Component} from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import './App.less';
import NewNote from "./components/NewNote";
import Home from "./pages/Home";
import NoteDetail from "./components/NoteDetail";

class App extends Component{

  render() {
    return (
      <div className='App'>
          <Router>
              <Switch>
                  <Route path="/notes/create" component={NewNote} />
                  <Route  path="/notes/:id" component={NoteDetail}/>
                  <Route exact path="/" component={Home}/>
                  {/*<Route exact path="/" component={LeftPart}/>*/}

              </Switch>
          </Router>
      </div>
    );
  }
}

export default App;
