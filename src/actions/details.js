export const getNoteDetails = () => (dispatch) => {
    fetch("http://localhost:8080/api/posts")
        .then(response => response.json())
        .then(result => {
            console.log('response', result);
            dispatch({
                type: 'GET_DETAILS',
                payload: result
            })
        })
};

export const deleteDetails = (id, history) => {
    fetch(`http://localhost:8080/api/posts/${id}`, {method: 'DELETE'})
        .then(() => {
            history.push('/');
            dispatch({
                type: 'DELETE_DETAILS'
            })
        })
};
