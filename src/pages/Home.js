import {Link} from "react-router-dom";
import '../style/Home.less'
import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {getNoteDetails} from "../actions/details";
import {connect} from "react-redux";
import HeaderBar from "../components/HeaderBar";
import { MdAddToPhotos } from "react-icons/md";

class Home extends Component {
    componentDidMount() {
        this.props.getNoteDetails();
    }
    render() {
        const details = this.props.payload;
        return (
            <div className="home">
                <HeaderBar></HeaderBar>
                <ul className="homeUl">
                    {details.map(item => (<li key={item.id}>
                        <Link to={`/notes/${item.id}`}>{item.title}</Link>
                    </li>))}
                    <li>
                        <Link to='/notes/create'><MdAddToPhotos/></Link>
                    </li>
                </ul>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    payload: state.details.payload
});
const mapDispatchToProps = dispatch => bindActionCreators({
    getNoteDetails
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Home);
